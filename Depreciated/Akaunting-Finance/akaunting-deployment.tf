resource "kubernetes_deployment_v1" "akaunting" {
  metadata {
    name      = "akaunting"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      type = "service"
      env  = "prod"
      name = "akaunting"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "akaunting"
      }
    }
    template {
      metadata {
        name = "akaunting"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "akaunting"
          "app.kubernetes.io/name" = "akaunting"
        }
      }

      spec {
        security_context {
          run_as_user = "0"
        }
        volume {
          name = "uploads"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.akaunting.metadata[0].name
          }
        }
        container {
          name              = "akaunting"
          image             = "${local.image_repository}/akaunting"
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 8080
            name           = "default-port"
          }
          #          env {
          #            name  = "AKAUNTING_SETUP"
          #            value = "true"
          #          }

          security_context {
            run_as_user = "0"
          }
          #
          #          volume_mount {
          #            mount_path = "/var/www/html"
          #            name       = "uploads"
          #          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.akaunting.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.akaunting.metadata[0].name
              optional = false
            }
          }
          resources {
            requests = {
              cpu    = "512m"
              memory = "1Gi"
            }
            limits = {
              cpu    = "1024m"
              memory = "2Gi"
            }
          }
        }
      }
    }
  }
}
