resource "kubernetes_config_map_v1" "akaunting" {
  metadata {
    name      = "akaunting-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    APP_URL       = "https://finance.local"
    LOCALE        = "en-US"
    DB_PORT       = "3306"
    DB_HOST       = "${kubernetes_service_v1.db.metadata[0].name}.finance.svc.cluster.local"
    DB_PREFIX     = "aka_"
    COMPANY_NAME  = "Home"
    COMPANY_EMAIL = "admin@home.local"
    #    AKAUNTING_SETUP = "true"
  }
}