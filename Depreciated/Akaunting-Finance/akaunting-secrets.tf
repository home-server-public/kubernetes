resource "kubernetes_secret_v1" "akaunting" {
  metadata {
    name      = "akaunting-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    APP_KEY        = "SomeRandomStringOf32CharsExactly"
    DB_NAME        = "akaunting"
    DB_USERNAME    = "admin"
    DB_PASSWORD    = "admin"
    ADMIN_EMAIL    = "admin@home.local"
    ADMIN_PASSWORD = "p@ssw0rd"
  }
}