resource "kubernetes_service_v1" "akaunting" {
  metadata {
    name      = "akaunting-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      env  = "prod"
      type = "service"
      name = "akaunting"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port = 80
    }
  }
}