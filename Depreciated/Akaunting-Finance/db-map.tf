resource "kubernetes_config_map_v1" "db" {
  metadata {
    name      = "db-map"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    MYSQL_DATABASE = "akaunting-db"
  }
}