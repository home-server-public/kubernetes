resource "kubernetes_namespace_v1" "this" {
  metadata {
    name = "finance"
    labels = {
      env = "prod"
      app = "finance"
    }
  }
}