resource "kubernetes_ingress_v1" "coder" {
  metadata {
    name      = "local-inventory-ingress"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["inventory.local"]
      secret_name = "inventory-local"
    }
    rule {
      host = "inventory.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.inventory.metadata[0].name
              port {
                number = 443
              }
            }
          }
        }
      }
    }
  }
}