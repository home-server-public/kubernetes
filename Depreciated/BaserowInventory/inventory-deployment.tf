resource "kubernetes_deployment_v1" "inventory" {
  metadata {
    name      = "inventory-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      type = "service"
      env  = "prod"
      name = "local-inventory"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "inventory"
      }
    }
    template {
      metadata {
        name = "local-inventory"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "inventory"
          "app.kubernetes.io/name" = "local-inventory"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.inventory.metadata[0].name
          }
        }
        container {
          port {
            container_port = 80
            name           = "secure-port"
          }
          env_from {
            config_map_ref {
              name = kubernetes_config_map_v1.inventory.metadata[0].name
            }
          }
          name              = "coder"
          image             = "${local.image_repository}/baserow"
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/baserow/data"
            name       = "data"
          }
        }
      }
    }
  }
}