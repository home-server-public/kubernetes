resource "kubernetes_config_map_v1" "inventory" {
  metadata {
    name      = "coder-config"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    BASEROW_PUBLIC_URL = "https://inventory.local"
    #    BASEROW_CADDY_ADDRESSES = "https://inventory.local"
    REDIS_URL    = "redis://${kubernetes_service_v1.redis-service.metadata[0].name}:6379"
    DATABASE_URL = "postgresql://admin:admin@${kubernetes_service_v1.db.metadata[0].name}/inventory?sslmode=disable"
  }
}