resource "kubernetes_service_v1" "inventory" {
  metadata {
    name      = "inventory-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      env  = "prod"
      type = "service"
      name = "inventory"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
      name = "inventory"
    }
    port {
      port        = 443
      target_port = 80
      name        = "default-port"
    }
  }
}