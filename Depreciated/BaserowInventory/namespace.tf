resource "kubernetes_namespace_v1" "this" {
  metadata {
    name = "local-inventory"
    labels = {
      env = "prod"
      app = "inventory"
    }
  }
}