resource "kubernetes_persistent_volume_v1" "redis" {
  metadata {
    name = "inventory-redis-claim-pv"
  }

  spec {
    access_modes = ["ReadWriteMany"]
    capacity = {
      storage = "1Gi"
    }
    persistent_volume_source {
      nfs {
        path   = local.service_nfs_path
        server = local.nfs
      }
    }
    storage_class_name = "nfs"
  }
}

resource "kubernetes_persistent_volume_v1" "redis-storage" {
  metadata {
    name = "inventory-redis-data-pv"
  }

  spec {
    access_modes = ["ReadWriteMany"]
    capacity = {
      storage = "1Gi"
    }
    persistent_volume_source {
      nfs {
        path   = local.service_nfs_path
        server = local.nfs
      }
    }
    storage_class_name = "nfs"
  }
}