# # Importing existing cluster after losing Source Code

# # Namespace
# import {
#   id = "starbase"
#   to = kubernetes_namespace_v1.this
# }

# # Ingress
# import {
#   id = "starbase/starbase-ingress"
#   to = kubernetes_ingress_v1.starbase
# }

# # Deployment
# import {
#   id = "starbase/starbase-deployment"
#   to = kubernetes_deployment_v1.starbase
# }

# # Service
# import {
#   id = "starbase/starbase-service"
#   to = kubernetes_service_v1.starbase
# }


# # Config Map
# import {
#   id = "starbase/starbase-sites"
#   to = kubernetes_config_map_v1.starbase
# }
