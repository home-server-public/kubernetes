resource "kubernetes_deployment_v1" "starbase" {
  metadata {
    name      = "starbase-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      type = "service"
      env  = "prod"
      name = "starbase"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
      }
    }
    template {
      metadata {
        name = "starbase"
        labels = {
          env  = "prod"
          type = "service"
          name = "starbase"
        }
      }
      spec {
        volume {
          name = "data"
          config_map {
            name = kubernetes_config_map_v1.starbase.metadata[0].name
          }
        }
        container {
          port {
            container_port = 4173
            name           = "default-port"
          }
          security_context {
            privileged = true
          }
          name              = "starbase"
          image             = "${local.image_repository}/starbase"
          image_pull_policy = "IfNotPresent"
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.starbase-env.metadata[0].name
              optional = false
            }
          }
          volume_mount {
            mount_path = "/app/src/config.json"
            sub_path   = "config.json"
            name       = "data"
          }
        }
      }
    }
  }
}
