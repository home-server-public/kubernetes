resource "kubernetes_config_map_v1" "starbase-env" {
  metadata {
    name      = "starbase-env-map"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    TITLE    = "Home Server"
    HEADER   = false
    HARDLINE = false
  }
}
