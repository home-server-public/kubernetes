resource "kubernetes_config_map_v1" "starbase" {
  metadata {
    name      = "starbase-sites"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    "config.json" = <<EOF
         [
  {
    "category": "SysAdmin",
    "services": [
      {
        "name": "NAS",
        "uri": "https://storage.local",
        "description": "Local NAS",
        "icon": "synology-drive",
        "iconColor": "blue-500"
      },
      {
        "name": "Patch Server",
        "uri": "https://patchserver.infra.local",
        "description": "Landscape Patch Server",
        "icon": "ubuntu",
        "iconColor": "orange-500"
      }
    ]
  },
  {
    "category": "DevOps",
    "services": [
      {
        "name": "Gitlab",
        "uri": "https://gitlab.local",
        "description": "Local Source Management",
        "icon": "gitlab",
        "iconColor": "blue-500"
      },
      {
        "name": "Database Management",
        "uri": "https://clouddb.local",
        "description": "Db Management",
        "icon": "cloudbeaver",
        "iconColor": "yellow-500"
      },
      {
        "name": "Dev Environments",
        "uri": "https://coder-platform.local",
        "description": "Create Development Environments with few clicks",
        "icon": "coder-light",
        "iconColor": "blue-500"
      },
      {
        "name": "Code Editor",
        "uri": "https://coder.local",
        "description": "Visual Studio Code in Browser",
        "icon": "code",
        "iconColor": "blue-500"
      }
    ]
  },
  {
    "category": "Security",
    "services": [
      {
        "name": "Passbolt",
        "uri": "https://secrets.local",
        "description": "Secrets Manager",
        "icon": "passwordpusher"
      }
    ]
  },
  {
    "category": "Personal",
    "services": [
      {
        "name": "Finance",
        "uri": "https://finance.local",
        "description": "Finance Solution",
        "icon": "firefly"
      },
      {
        "name": "File Share",
        "uri": "https://files.local",
        "description": "File Sharing and hosting",
        "icon": "pydio"
      },
      {
        "name": "Bookmarks",
        "uri": "https://bookmarks.local",
        "description": "Bookmarks Tracker",
        "icon": "gitbook"
      },
      {
        "name": "Inventory",
        "uri": "https://inventory.local",
        "description": "Local Inventory Tracker",
        "icon": "baserow"
      },
      {
        "name": "Time Tracker",
        "uri": "https://timetracker.local",
        "description": "Time Tracker",
        "icon": "kimai"
      }
    ]
  }
]
    EOF
  }
}
