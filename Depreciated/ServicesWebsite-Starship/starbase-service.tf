resource "kubernetes_service_v1" "starbase" {
  metadata {
    name      = "starbase-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      env  = "prod"
      type = "service"
      name = "starbase"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
      name = "starbase"
    }
    port {
      port        = 80
      target_port = 4173
      name        = "default-port"
    }
  }
}