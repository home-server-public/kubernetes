resource "kubernetes_deployment_v1" "citizen" {
  metadata {
    name      = "citizen-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "citizen"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "citizen"
      }
    }
    template {
      metadata {
        name   = "citizen"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "citizen"
          "app.kubernetes.io/name" = "citizen"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.citizen.metadata[0].name
          }
        }
        container {
          port {
            container_port = 3000
            name           = "default-port"
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.citizen.metadata[0].name
              optional = false
            }
          }
          name              = "citizen"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/opt/citizen/workspace"
            name       = "data"
          }
        }
      }
    }
  }
}
