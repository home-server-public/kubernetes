resource "kubernetes_config_map_v1" "citizen" {
  metadata {
    name      = "citizen-config-map"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }

  data = {
    CITIZEN_ADDR          = "https://private-tf.local"
    CITIZEN_DATABASE_TYPE = "mongodb"
    CITIZEN_DATABASE_URL  = "mongodb://admin:admin@mongo:27017"
    CITIZEN_STORAGE       = "file"
    CITIZEN_STORAGE_PATH  = "/opt/citizen/workspace"
  }
}