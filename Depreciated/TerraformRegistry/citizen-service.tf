resource "kubernetes_service_v1" "citizen" {
  metadata {
    name      = "citizen-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "citizen"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 80
      target_port = 3000
      name        = "default-port"
    }
  }
}