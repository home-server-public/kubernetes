resource "kubernetes_persistent_volume_v1" "citizen" {
  metadata {
    name   = "citizen-pv"
    labels = {
      app = "citizen"
    }
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
    capacity           = {
      storage = "1Gi"
    }
    persistent_volume_source {
      nfs {
        path      = local.service_nfs_path
        server    = local.nfs
        read_only = false
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "citizen" {
  metadata {
    name      = "citizen-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    selector {
      match_labels = {
        app = "citizen"
      }
    }
    resources {
      limits = {
        storage = "2Gi"
      }
      requests = {
        storage = "1Gi"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}