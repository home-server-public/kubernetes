resource "kubernetes_network_policy_v1" "db" {
  metadata {
    name      = "db-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress"]
    pod_selector {
      match_labels = {
        env  = "prod"
        type = "db"
      }
    }
    ingress {
      ports {
        port     = 27017
        protocol = "TCP"
      }
      from {
        namespace_selector {
          match_labels = {
            env = "prod"
            app = "citizen"
          }
        }
      }
      from {
        namespace_selector {
          match_labels = {
            app = "citizen"
          }
        }
      }
    }
  }
}