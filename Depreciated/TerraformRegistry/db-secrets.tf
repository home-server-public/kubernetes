resource "kubernetes_secret_v1" "db" {
  metadata {
    name      = "db-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    MONGO_INITDB_ROOT_USERNAME = "admin"
    MONGO_INITDB_ROOT_PASSWORD = "admin"
  }
}