resource "kubernetes_service_v1" "db" {
  metadata {
    name      = "citizen-db"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "db"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "db"
    }
    port {
      port        = 27017
      target_port = 27017
      protocol    = "TCP"
      name        = "default-port"
    }
  }
}