resource "kubernetes_ingress_v1" "citizen" {
  metadata {
    name        = "citizen-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["private-tf.local"]
      secret_name = "private-tf-local"
    }
    rule {
      host = "private-tf.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.citizen.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}