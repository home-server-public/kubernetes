locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_nfs_path     = "/volume1/k8s/citizen/service"
  db_nfs_path          = "/volume1/k8s/citizen/db"
  db_image_digest      = "sha256:87feebc1a3d1632bcd0fa138631360540c72d8ee0d0069a998db773fd7349cfc"
  db_image             = "${local.image_repository}/mongo@${local.db_image_digest}"
  service_image_digest = "sha256:2fcc7cb978af444f15933cfd79d38defab8c1b2faeb178367a6e5b8334cea290"
  service_image        = "${local.image_repository}/citizen@${local.service_image_digest}"
}