resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "citizen"
    labels = {
      env = "prod"
      app = "citizen"
    }
  }
}