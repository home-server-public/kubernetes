resource "kubernetes_ingress_v1" "starbase" {
  metadata {
    name        = "file-share-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["file-share.local"]
      secret_name = "file-share-local"
    }
    rule {
      host = "file-share.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.this.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}