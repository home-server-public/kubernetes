resource "kubernetes_config_map_v1" "starbase" {
  metadata {
    name      = "file-share-sites"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    "default" = <<EOF
         server {
	listen 80 default_server;
	server_name file-share.local;

	location / {
        root /var/www/html;
        index index.html;
        add_header Content-disposition "attachment; filename=$1";
        try_files $uri $uri/;
	}

    location /downloads/ {
        root /usr/share/nginx/html/downloads;
        Content-Type: application/octet-stream;
        add_header Content-disposition "attachment; filename=$1";
        try_files $uri $uri/ /downloads/$uri/;
	}
}
    EOF
  }
}
