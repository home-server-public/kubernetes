resource "kubernetes_network_policy_v1" "gitlab" {
  metadata {
    name      = "webserver-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress", "Egress"]
    pod_selector {
      match_labels = {
        env  = "prod"
        type = "service"
      }
    }
    ingress {
      ports {
        port     = 80
        protocol = "TCP"
      }
      ports {
        port     = 443
        protocol = "TCP"
      }
    }
    egress {}
  }
}