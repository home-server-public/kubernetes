resource "kubernetes_service_v1" "this" {
  metadata {
    name      = "file-share-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "file-share"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
      name = "file-share"
    }
    port {
      port = 80
      name = "default-port"
    }
  }
}