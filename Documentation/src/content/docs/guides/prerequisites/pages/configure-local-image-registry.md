---
title: Local Image Registry
description: A guide for configuring Local Container Registry.
---

## About

There are few things that you can do if you choose to host a local image registry on your NAS.
However; configuring your NAS storage is outside the scope of this setup as it varies between different setups of what you have locally.

## Getting Started

When you pull an image from the docker official registry, you are pulling over HTTPS,
when using a local registry you will be pulling over HTTP. In this guide you will see how we can configure Microk8s
to allow insecure registries.

### Configuring Microk8s

Microk8s 1.23 and newer use separate `host.toml` files for each registry. For this guide we will assume your registry
is reachable on this IP `10.42.0.22` and is listening on port `5050`

First, let's begin with creating a new directory for your registry:

```bash
sudo mkdir -p /var/snap/microk8s/current/args/certs.d/192.168.1.102:5050
```

Then we will create the host.toml file

```bash
sudo touch /var/snap/microk8s/current/args/certs.d/192.168.1.102:5050/hosts.toml
```

Edit hosts.toml file to include the following

```toml
server = "http://192.168.1.102:5050"

[host."http://192.168.1.102:5050"]
capabilities = ["pull", "resolve"]
```

Restart Microk8s for the new configuration to be loaded

```bash
microk8s stop && microk8s start
```

### Pushing Images to Local Image Registry

Now that you have the local registry trusted by Microk8s let's push our first image

Pull an image from the official docker registry

```bash
docker pull busybox
```

Tag the image you want to push to your local registry

```bash
docker tag busybox 192.168.1.102:5050/busybox
```

Push the tagged image to your local registry

```bash
docker push 192.168.1.102:5050/busybox
```

That's it! You are now ready to use your local registry for your infrastructure
