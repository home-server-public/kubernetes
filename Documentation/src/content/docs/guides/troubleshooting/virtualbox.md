---
title: Troubleshoot VirtualBox
description: Troubleshoot common errors
---

This section includes troubleshooting some of the common issues I faced when using VirtualBox. This guide assumes you are using Ubuntu Server, for other OS please refer to their official guides.

## Network Adapters

Adding new network adapter to guest OS after OS was installed

### What went wrong

I added a new network adapter to the guest VM but it doesn’t show up when I run `ifconfig`

### How to solve the issue

First you need to get the new adapter name by running this command: `ip link show` the output will be something like:

```bash
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:15:74:ac brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 08:00:27:6d:d4:30 brd ff:ff:ff:ff:ff:ff
```

- Next go to folder `/etc/netplan/`
- Update the file `00-installer-config.yaml`
- Add new entry so that the file looks like this

```yaml
# This is the network config written by 'subiquity'
network:
  ethernets:
    enp0s3:
      dhcp4: true
    enp0s8: # Add this
      dhcp4: true # Add this to make it auto assign IP
  version: 2
```

---

## Network Reachability

For my VMs to keep communication internal I have setup a NAT Network. If you have chosen to do the same you might face issues sometimes.

### What went wrong

All pods in the cluster are failing to mount their volumes because NAS is not reachable

### How to solve the issue

Run the following commands on your host OS

```bash
# Replace LocalLab with your NAT Network name
VBoxManage natnetwork stop --netname LocalLab
VBoxManage natnetwork start --netname LocalLab
```
