---
title: My Tools
description: A reference page for useful links.
---

I have developed few tools to help me get information that I care about instantly even if there are alternatives out there.

## Tools

- [Docker Registry Explorer](https://gitlab.com/open-source54751/docker-registry-explorer.git)
  Simple flutter app to explore private registry docker
  ![Docker Registry Explorer](../../../assets/docker.png)
- [Project Verify](https://gitlab.com/open-source54751/project-verify)
  Simple CLI tool to show available namespaces and pods status for each namespace. Written in Go and uses [tview UI Library](https://github.com/rivo/tview)
  ![Project Verify](../../../assets/verify.png)
