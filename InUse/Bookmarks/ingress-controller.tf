resource "kubernetes_ingress_v1" "servas" {
  metadata {
    name        = "servas-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
      "nginx.ingress.kubernetes.io/proxy-body-size" : "1024m"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["bookmarks.local"]
      secret_name = "bookmarks-local"
    }
    rule {
      host = "bookmarks.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.servas.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}