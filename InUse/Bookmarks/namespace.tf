resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "bookmarks-tracker"
    labels = {
      env = "prod"
      app = "bookmarks"
    }
  }
}