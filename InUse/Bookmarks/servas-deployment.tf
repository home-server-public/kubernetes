resource "kubernetes_deployment_v1" "servas" {
  metadata {
    name      = "servas"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "servas"

    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "servas"
      }
    }
    template {
      metadata {
        name   = "servas"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "servas"
          "app.kubernetes.io/name" = "servas"
        }
      }
      spec {
        volume {
          name = "data"
          config_map {
            name = kubernetes_config_map_v1.servas.metadata[0].name
          }
        }
        container {
          name              = "servas"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 8001
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.servas.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.servas.metadata[0].name
              optional = false
            }
          }
          volume_mount {
            mount_path = "/var/www/html/.env"
            sub_path   = ".env"
            name       = "data"
          }
          resources {
            requests = {
              cpu    = "256m"
              memory = "1Gi"
            }
            limits = {
              cpu    = "1024m"
              memory = "2Gi"
            }
          }
        }
      }
    }
  }
}
