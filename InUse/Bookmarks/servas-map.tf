resource "kubernetes_config_map_v1" "servas" {
  metadata {
    name      = "servas-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    DB_CONNECTION = "mysql"
    DB_HOST       = kubernetes_service_v1.db.metadata[0].name
    DB_PORT       = "3306"
    DB_DATABASE   = "servas"
    APP_NAME      = "Servas"
    APP_URL       = "https://bookmarks.local"
    APP_ENV       = "production"
    APP_DEBUG     = "false"
    ".env"        = <<EOF
APP_NAME=Servas
APP_ENV=production
APP_KEY=base64:dwDicJlu1awcjRBHx7NFZPaOEkX9KWwZYoVLaQL+a2c=
APP_DEBUG=false
APP_URL=https://bookmarks.local

SERVAS_ENABLE_REGISTRATION=true

# MySQL
DB_CONNECTION=mysql
DB_HOST=servas-db.bookmarks-tracker.svc
DB_PORT=3306
DB_DATABASE=servas
DB_USERNAME=servas
DB_PASSWORD=password

EOF
  }
}