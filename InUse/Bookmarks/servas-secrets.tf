resource "kubernetes_secret_v1" "servas" {
  metadata {
    name      = "servas-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    DB_USERNAME = "servas"
    DB_PASSWORD = "servas"
  }
}