resource "kubernetes_deployment_v1" "cloudbeaver" {
  metadata {
    name      = "cloudbeaver-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "cloudbeaver"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "cloudbeaver"
      }
    }
    template {
      metadata {
        name   = "cloudbeaver"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "cloudbeaver"
          "app.kubernetes.io/name" = "cloudbeaver"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.cloudbeaver.metadata[0].name
          }
        }
        container {
          port {
            container_port = 8978
            name           = "default-port"
          }
          security_context {
            privileged = true
          }
          name              = "cloudbeaver"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/opt/cloudbeaver/workspace"
            name       = "data"
          }

          resources {
            limits = {
              cpu    = 1
              memory = "512Mi"
            }
            requests = {
              cpu    = 0.5
              memory = "256Mi"
            }
          }
        }
      }
    }
  }
}
