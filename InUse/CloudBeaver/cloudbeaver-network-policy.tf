resource "kubernetes_network_policy_v1" "this" {
  metadata {
    name      = "cloudbeaver-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress"]
    pod_selector {
      match_labels = {
        env  = "prod"
        type = "service"
        name = "cloudbeaver"
      }
    }
    ingress {
      ports {
        port     = 8978
        protocol = "TCP"
      }
    }
  }
}