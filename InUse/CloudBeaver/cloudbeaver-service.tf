resource "kubernetes_service_v1" "cloudbeaver" {
  metadata {
    name      = "cloudbeaver-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "cloudbeaver"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 443
      target_port = 8978
      name        = "default-port"
    }
  }
}