#!/bin/bash

echo Writing Plan to plan.out

terraform plan -out=plan.out

echo Converting plan to JSON

terraform show -json plan.out >plan.json

echo Generating Visual Plan

terraform-visual --plan plan.json

echo Cleaning Up

rm plan.json
rm plan.out

echo DONE!

open terraform-visual-report/index.html
