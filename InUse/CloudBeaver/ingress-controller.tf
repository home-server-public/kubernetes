resource "kubernetes_ingress_v1" "cloudbeaver" {
  metadata {
    name        = "cloudbeaver-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["clouddb.local"]
      secret_name = "clouddb-local"
    }
    rule {
      host = "clouddb.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.cloudbeaver.metadata[0].name
              port {
                number = 443
              }
            }
          }
        }
      }
    }
  }
}