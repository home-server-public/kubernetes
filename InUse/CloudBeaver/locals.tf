locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_nfs_path     = "/volume1/k8s/cloudbeaver/service"
  service_image_digest = "sha256:027ced2bd5820fcca225ab71d5367777597cd4cd1eb872466138de6d9e85a57e"
  service_image        = "${local.image_repository}/cloudbeaver@${local.service_image_digest}"
}