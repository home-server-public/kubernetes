resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "cloudbeaver"
    labels = {
      env = "prod"
      app = "cloudbeaver"
    }
  }
}