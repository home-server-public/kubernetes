resource "kubernetes_deployment_v1" "coder" {
  metadata {
    name      = "coder-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "coder"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "coder"
      }
    }
    template {
      metadata {
        name   = "coder"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "coder"
          "app.kubernetes.io/name" = "vs-code"
        }
      }
      spec {
        node_selector = {
          size = "medium"
        }
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.coder.metadata[0].name
          }
        }
        container {
          port {
            container_port = 8443
            name           = "default-port"
          }
          name              = "coder"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/config"
            name       = "data"
          }
          env {
            name  = "PUID"
            value = 1000
          }
          env {
            name  = "PGID"
            value = 1000
          }
          env {
            name  = "TZ"
            value = "Etc/UTC"
          }
          env {
            name  = "PROXY_DOMAIN"
            value = "coder.local"
          }
          #          env {
          #            name  = "DOCKER_MODS"
          #            value = "linuxserver/mods:code-server-extension-arguments|ghcr.io/ivanmorenoj/lsio-mods:code-server-ws"
          #          }
          env {
            name  = "PASSWORD"
            value = "p@ssw0rd"
          }
          env {
            name  = "SUDO_PASSWORD"
            value = "p@ssw0rd"
          }
        }
      }
    }
  }
}