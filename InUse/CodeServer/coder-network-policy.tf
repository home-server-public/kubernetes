resource "kubernetes_network_policy_v1" "gitlab" {
  metadata {
    name      = "coder-server-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress", "Egress"]
    pod_selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "coder"
      }
    }
    ingress {
      ports {
        port     = 8443
        protocol = "TCP"
      }
    }
    egress {}
  }
}