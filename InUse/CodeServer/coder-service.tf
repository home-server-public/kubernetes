resource "kubernetes_service_v1" "coder" {
  metadata {
    name      = "coder-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "coder"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 443
      target_port = 8443
      name        = "default-port"
    }
  }
}