resource "kubernetes_persistent_volume_v1" "coder" {
  metadata {
    name   = "coder-pv"
    labels = {
      app = "coder"
    }
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
    capacity           = {
      storage = "10Gi"
    }
    persistent_volume_source {
      nfs {
        path      = local.service_nfs_path
        server    = local.nfs
        read_only = false
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "coder" {
  metadata {
    name      = "coder-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    selector {
      match_labels = {
        app = "coder"
      }
    }
    resources {
      limits = {
        storage = "20Gi"
      }
      requests = {
        storage = "10Gi"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}