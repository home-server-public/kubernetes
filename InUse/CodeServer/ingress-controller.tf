resource "kubernetes_ingress_v1" "coder" {
  metadata {
    name        = "coder-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["coder.local"]
      secret_name = "coder-local"
    }
    rule {
      host = "coder.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.coder.metadata[0].name
              port {
                number = 443
              }
            }
          }
        }
      }
    }
  }
}