locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_image_digest = "sha256:e4e68fec2985f0d545034675d1c662f031f0444e40f7d51f30908126bb318a07"
  service_image        = "${local.image_repository}/code-server@${local.service_image_digest}"
  service_nfs_path     = "/volume1/k8s/coder/service"
}