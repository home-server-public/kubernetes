resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "coder"
    labels = {
      env = "prod"
    }
  }
}