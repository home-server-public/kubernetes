resource "kubernetes_deployment_v1" "coder" {
  metadata {
    name      = "coder-platform-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "coder"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "coder"
      }
    }
    template {
      metadata {
        name   = "coder"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "coder"
          "app.kubernetes.io/name" = "coder"
        }
      }
      spec {
        node_selector = {
          size = "medium"
        }
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.coder.metadata[0].name
          }
        }
        security_context {
          fs_group        = "1000"
          run_as_group    = "1000"
          run_as_user     = "1000"
          run_as_non_root = true
        }
        service_account_name = "coder"
        container {
          port {
            container_port = 3000
            name           = "default-port"
          }
          env_from {
            config_map_ref {
              name = kubernetes_config_map_v1.coder.metadata[0].name
            }
          }
          name              = "coder"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/home/coder/"
            name       = "data"
          }
        }
      }
    }
  }
}