resource "kubernetes_config_map_v1" "coder" {
  metadata {
    name      = "coder-config"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    CODER_HTTP_ADDRESS      = "0.0.0.0:3000"
    CODER_ACCESS_URL        = "https://coder-platform.local"
    https_proxy             = "10.136.113.13:3128"
    http_proxy              = "10.136.113.13:3128"
    CODER_PG_CONNECTION_URL = "postgresql://admin:admin@${kubernetes_service_v1.db.metadata[0].name}/coder?sslmode=disable"
  }
}