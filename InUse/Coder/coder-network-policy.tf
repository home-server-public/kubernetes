resource "kubernetes_network_policy_v1" "this" {
  metadata {
    name      = "coder-platform-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress"]
    pod_selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "coder"
      }
    }
    ingress {
      ports {
        port     = 3000
        protocol = "TCP"
      }
    }
  }
}