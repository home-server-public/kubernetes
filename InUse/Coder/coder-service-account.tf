resource "kubernetes_service_account_v1" "coder" {
  metadata {
    name      = "coder"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
}

resource "kubernetes_role_binding_v1" "coder" {
  metadata {
    name      = "coder"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.coder.metadata[0].name
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    api_group = ""
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "coder"
  }
}

resource "kubernetes_role_v1" "coder" {
  metadata {
    name      = "coder"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  rule {
    api_groups = ["", "apps"]
    resources  = ["*"]
    verbs      = ["*"]
  }
}

resource "kubernetes_cluster_role_v1" "coder" {
  metadata {
    name = "coder-cluster-role"
  }
  rule {
    verbs      = ["*"]
    api_groups = ["", "apps"]
    resources  = ["*"]
  }
}

resource "kubernetes_cluster_role_binding_v1" "coder" {
  metadata {
    name = "coder-cluster-role-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = kubernetes_cluster_role_v1.coder.metadata[0].name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.coder.metadata[0].name
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
}