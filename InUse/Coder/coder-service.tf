resource "kubernetes_service_v1" "coder" {
  metadata {
    name      = "coder-platform-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "coder"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
      name = "coder"
    }
    port {
      port        = 443
      target_port = 3000
      name        = "default-port"
    }
  }
}