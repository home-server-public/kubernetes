locals {
  image_repository = "192.168.1.102:5050"
  nfs              = "192.168.1.102"
  db_image_digest      = "sha256:0e63d93e8450edde683bafc37ebb880f9fa30c18608bb69d365024a842a90190"
  service_image_digest = "sha256:fa675ad8ff5170a11a3246d79bb2b1f44ffe1010c58c11691222bab9050f27d9"
  db_image             = "${local.image_repository}/postgres@${local.db_image_digest}"
  service_image        = "${local.image_repository}/coder@${local.service_image_digest}"
  db_nfs_path      = "/volume1/k8s/coder-platform/db"
  service_nfs_path = "/volume1/k8s/coder-platform/service"
}