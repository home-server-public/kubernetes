resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "coder-platform"
    labels = {
      env = "prod"
      app = "coder-platform"
    }
  }
}