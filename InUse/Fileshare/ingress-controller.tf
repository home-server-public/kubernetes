resource "kubernetes_ingress_v1" "cells" {
  metadata {
    name        = "cells-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
      "nginx.ingress.kubernetes.io/proxy-body-size" : "1024m"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["files.local"]
      secret_name = "files-local"
    }
    rule {
      host = "files.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.cells.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}