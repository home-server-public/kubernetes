locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  db_nfs_path          = "/volume1/k8s/cells/db"
  service_nfs_path     = "/volume1/k8s/cells/service"
  db_image_digest      = "sha256:4c59990ae8966b52330f64f5202a6e9bd7e1d6bfb89dd69583e24d9d0e9bcd26"
  service_image_digest = "sha256:0c0631d09f272077b9cee6cacbe125ec5c94382f7dd1f28246f7f9fd30d9431c"
  db_image = "${local.image_repository}/mariadb@${local.db_image_digest}"
  service_image = "${local.image_repository}/cells@${local.service_image_digest}"
}