resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "file-storage"
    labels = {
      env = "prod"
      app = "fileshare"
    }
  }
}