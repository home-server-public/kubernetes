resource "kubernetes_deployment_v1" "cells" {
  metadata {
    name      = "cells"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "cells"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "cells"
      }
    }
    template {
      metadata {
        name   = "cells"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "cells"
          "app.kubernetes.io/name" = "fileshare"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.cells.metadata[0].name
          }
        }
        container {
          name              = "cells"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 443
            name           = "default-port"
          }
          volume_mount {
            mount_path = "/var/cells"
            name       = "data"
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.cells.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.cells.metadata[0].name
              optional = false
            }
          }
          resources {
            requests = {
              cpu    = "512m"
              memory = "1Gi"
            }
            limits = {
              cpu    = "1024m"
              memory = "2Gi"
            }
          }
        }
      }
    }
  }
}
