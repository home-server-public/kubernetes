resource "kubernetes_config_map_v1" "cells" {
  metadata {
    name      = "cells-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    CELLS_SITE_EXTERNAL     = "https://files.local"
    CELLS_SITE_NO_TLS       = "1"
    DATASOURCE_DEFAULT_HOST = "cells-db"
    MYSQL_DATABASE          = "cells"
  }
}