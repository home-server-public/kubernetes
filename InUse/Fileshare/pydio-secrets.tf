resource "kubernetes_secret_v1" "cells" {
  metadata {
    name      = "cells-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    MYSQL_PASSWORD      = "admin"
    MYSQL_ROOT_PASSWORD = "admin"
  }
}