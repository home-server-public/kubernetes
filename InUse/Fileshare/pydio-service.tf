resource "kubernetes_service_v1" "cells" {
  metadata {
    name      = "cells-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "cells"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 80
      target_port = 8080
      name        = "default-port"
    }
  }
}