resource "kubernetes_persistent_volume_v1" "cells" {
  depends_on = [kubernetes_persistent_volume_v1.db, kubernetes_persistent_volume_claim_v1.db]
  metadata {
    name = "cells-pv"
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
    capacity           = {
      storage = "1Ti"
    }
    persistent_volume_source {
      nfs {
        path      = local.service_nfs_path
        server    = local.nfs
        read_only = false
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "cells" {
  depends_on = [kubernetes_persistent_volume_v1.db, kubernetes_persistent_volume_claim_v1.db]
  metadata {
    name      = "cells-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    resources {
      limits = {
        storage = "2Ti"
      }
      requests = {
        storage = "1Ti"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}