resource "kubernetes_deployment_v1" "firefly" {
  metadata {
    name      = "firefly"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "firefly"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "firefly"
      }
    }
    template {
      metadata {
        name   = "firefly"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "firefly"
          "app.kubernetes.io/name" = "firefly"
        }
      }
      spec {
        container {
          name              = "firefly"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 8080
            name           = "default-port"
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.firefly.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.firefly.metadata[0].name
              optional = false
            }
          }
          resources {
            requests = {
              cpu    = "512m"
              memory = "1Gi"
            }
            limits = {
              cpu    = "1024m"
              memory = "2Gi"
            }
          }
        }
      }
    }
  }
}
