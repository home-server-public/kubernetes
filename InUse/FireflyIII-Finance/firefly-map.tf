resource "kubernetes_config_map_v1" "firefly" {
  metadata {
    name      = "firefly-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    APP_URL         = "https://finance.local"
    TRUSTED_PROXIES = "*"
    DB_HOST         = "firefly-db"
    DB_DATABASE     = "firefly"
  }
}