resource "kubernetes_secret_v1" "firefly" {
  metadata {
    name      = "firefly-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    APP_KEY     = "SomeRandomStringOf32CharsExactly"
    DB_USERNAME = "admin"
    DB_PASSWORD = "admin"
  }
}