resource "kubernetes_service_v1" "firefly" {
  metadata {
    name      = "firefly-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "firefly"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 80
      target_port = 8080
      name        = "default-port"
    }
  }
}