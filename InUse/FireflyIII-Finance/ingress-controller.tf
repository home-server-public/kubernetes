resource "kubernetes_ingress_v1" "firefly" {
  metadata {
    name        = "firefly-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["finance.local"]
      secret_name = "finance-local"
    }
    rule {
      host = "finance.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.firefly.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}