locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  db_nfs_path          = "/volume1/k8s/firefly/db"
  db_image_digest      = "sha256:4c59990ae8966b52330f64f5202a6e9bd7e1d6bfb89dd69583e24d9d0e9bcd26"
  service_image_digest = "sha256:3c8fa947518427cf5a00ce5d82459ff8932b097d515e875ea08aa4b6c29eda12"
  db_image             = "${local.image_repository}/mariadb@${local.db_image_digest}"
  service_image        = "${local.image_repository}/firefly@${local.service_image_digest}"
}