resource "kubernetes_deployment_v1" "db" {
  depends_on = [kubernetes_config_map_v1.db]
  metadata {
    name      = "gitlab-db"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "db"
      env  = "prod"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "db"
        env  = "prod"
      }
    }
    template {
      metadata {
        name   = "gitlab-db"
        labels = {
          type = "db"
          env  = "prod"
        }
      }
      spec {
        affinity {
          node_affinity {
            required_during_scheduling_ignored_during_execution {
              node_selector_term {
                match_expressions {
                  key      = "size"
                  operator = "In"
                  values   = ["large"]
                }
              }
            }
          }
        }
        security_context {
          fs_group = "2000"
        }
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.db.metadata[0].name
          }
        }
        container {
          name              = "postgres"
          image             = "${local.image_repository}/postgres"
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/var/lib/postgresql/data"
            name       = "data"
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.db.metadata[0].name
              optional = false
            }
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.db.metadata[0].name
              optional = false
            }
          }
          port {
            container_port = 5432
            protocol       = "TCP"
          }
        }
      }
    }
  }
}
