resource "kubernetes_config_map_v1" "db" {
  metadata {
    name      = "db-map"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    POSTGRES_DB = "gitlabhq_production"
    PGDATA      = "/var/lib/postgresql/data/pgdata"
  }
}