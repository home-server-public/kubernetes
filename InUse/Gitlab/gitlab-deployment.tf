resource "kubernetes_deployment_v1" "gitlab" {
  metadata {
    name      = "gitlab"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "deployment"
      env  = "prod"
      name = "gitlab"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
      }
    }
    template {
      metadata {
        name   = "gitlab"
        labels = {
          env  = "prod"
          type = "service"
        }
      }
      spec {
        affinity {
          node_affinity {
            required_during_scheduling_ignored_during_execution {
              node_selector_term {
                match_expressions {
                  key      = "size"
                  operator = "In"
                  values   = ["large"]
                }
              }
            }
          }
        }
        #     node_selector = {
        #          size = large
        #        }
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.gitlab.metadata[0].name
          }
        }
        container {
          security_context {
            privileged = true
          }
          port {
            container_port = 80
            name           = "default-port"
          }
          name              = "gitlab"
          image             = "${local.image_repository}/gitlab-ce"
          image_pull_policy = "IfNotPresent"
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.gitlab.metadata[0].name
              optional = false
            }
          }
          volume_mount {
            mount_path = "/etc/gitlab"
            name       = "data"
          }
          volume_mount {
            mount_path = "/var/log/gitlab"
            name       = "data"
          }
          volume_mount {
            mount_path = "/var/opt/gitlab"
            name       = "data"
          }
        }
      }
    }
  }
}
