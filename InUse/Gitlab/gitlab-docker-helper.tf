#resource "kubernetes_deployment_v1" "docker" {
#  depends_on = [kubernetes_persistent_volume_claim_v1.docker]
#  metadata {
#    name      = "gitlab-docker-deployment"
#    namespace = kubernetes_namespace_v1.this.metadata[0].name
#  }
#
#  spec {
#    selector {
#      match_labels = {
#        env  = "prod"
#        type = "ci-runner"
#        name = "gitlab-docker"
#      }
#    }
#    template {
#      metadata {
#        name   = "gitlab-docker"
#        labels = {
#          env  = "prod"
#          type = "ci-runner"
#          name = "gitlab-docker"
#        }
#      }
#      spec {
#        volume {
#          name = "tls"
#          secret {
#            secret_name = "gitlab-local"
#          }
#        }
#        volume {
#          name = "data"
#          persistent_volume_claim {
#            claim_name = kubernetes_persistent_volume_claim_v1.docker.metadata[0].name
#          }
#        }
#        volume {
#          name = "map"
#          config_map {
#            name = kubernetes_config_map_v1.gitlab.metadata[0].name
#          }
#        }
#        container {
#          name  = "gitlab-docker"
#          image = "${local.image_repository}/docker"
#          volume_mount {
#            mount_path = "/etc/ssl/certs"
#            name       = "tls"
#          }
#          security_context {
#            run_as_user = "0"
#            privileged  = true
#          }
#        }
#      }
#    }
#  }
#}
#
#
#resource "kubernetes_persistent_volume_v1" "docker" {
#  metadata {
#    name = "gitlab-docker-pv"
#  }
#  spec {
#    storage_class_name = "nfs"
#    access_modes       = ["ReadWriteMany"]
#    persistent_volume_source {
#      nfs {
#        path   = local.docker_nfs_path
#        server = local.nfs
#      }
#    }
#    capacity = {
#      storage = "1Gi"
#    }
#  }
#}
#
#resource "kubernetes_persistent_volume_claim_v1" "docker" {
#  depends_on = [kubernetes_persistent_volume_v1.docker]
#  metadata {
#    name      = "gitlab-docker-pvc"
#    namespace = kubernetes_namespace_v1.this.metadata[0].name
#  }
#  spec {
#    access_modes       = ["ReadWriteMany"]
#    storage_class_name = "nfs"
#    resources {
#      limits = {
#        storage = "2Gi"
#      }
#      requests = {
#        storage = "1Gi"
#      }
#    }
#  }
#}