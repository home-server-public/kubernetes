#resource "kubernetes_deployment_v1" "runner" {
#  depends_on = [kubernetes_persistent_volume_claim_v1.runner]
#  metadata {
#    name      = "gitlab-runner-deploymenet"
#    namespace = kubernetes_namespace_v1.this.metadata[0].name
#  }
#
#  spec {
#    selector {
#      match_labels = {
#        env  = "prod"
#        type = "ci-runner"
#        name = "gitlab-runner"
#      }
#    }
#    template {
#      metadata {
#        name   = "gitlab-runner"
#        labels = {
#          env  = "prod"
#          type = "ci-runner"
#          name = "gitlab-runner"
#        }
#      }
#      spec {
#        volume {
#          name = "tls"
#          secret {
#            secret_name = "gitlab-local"
#          }
#        }
#        volume {
#          name = "data"
#          persistent_volume_claim {
#            claim_name = kubernetes_persistent_volume_claim_v1.runner.metadata[0].name
#          }
#        }
#        container {
#          security_context {
#            run_as_user = "0"
#            privileged  = true
#          }
#          name  = "gitlab-runner"
#          image = "${local.image_repository}/gitlab-runner-docker:v3"
#          volume_mount {
#            mount_path = "/etc/ssl/certs"
#            name       = "tls"
#          }
#          volume_mount {
#            mount_path = "/etc/gitlab-runner"
#            name       = "data"
#          }
#        }
#      }
#    }
#  }
#}
#
#
#resource "kubernetes_persistent_volume_v1" "runner" {
#  metadata {
#    name = "gitlab-runner-pv"
#  }
#  spec {
#    storage_class_name = "nfs"
#    access_modes       = ["ReadWriteMany"]
#    persistent_volume_source {
#      nfs {
#        path   = local.runner_nfs_path
#        server = local.nfs
#      }
#    }
#    capacity = {
#      storage = "1Gi"
#    }
#  }
#}
#
#resource "kubernetes_persistent_volume_claim_v1" "runner" {
#  depends_on = [kubernetes_persistent_volume_v1.runner]
#  metadata {
#    name      = "gitlab-runner-pvc"
#    namespace = kubernetes_namespace_v1.this.metadata[0].name
#  }
#  spec {
#    access_modes       = ["ReadWriteMany"]
#    storage_class_name = "nfs"
#    resources {
#      limits = {
#        storage = "2Gi"
#      }
#      requests = {
#        storage = "1Gi"
#      }
#    }
#  }
#}