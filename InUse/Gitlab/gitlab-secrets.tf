resource "kubernetes_secret_v1" "gitlab" {
  metadata {
    name      = "gitlab-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    POSTGRES_DB       = "gitlabhq_production"
    POSTGRES_USER     = "gitlab"
    POSTGRES_PASSWORD = "gitlab"
  }
}