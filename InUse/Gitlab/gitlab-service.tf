resource "kubernetes_service_v1" "gitlab" {
  metadata {
    name      = "gitlab-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels = {
      env  = "prod"
      type = "service"
      name = "gitlab-service"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }
    port {
      port = 80
      name = "default-port"
    }
  }
}
