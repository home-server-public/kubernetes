resource "kubernetes_persistent_volume_v1" "gitlab" {
  depends_on = [kubernetes_persistent_volume_claim_v1.db, kubernetes_persistent_volume_v1.db]
  metadata {
    name = "gitlab-pv"
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
    capacity           = {
      storage = "8Gi"
    }
    persistent_volume_source {
      nfs {
        path      = local.service_nfs_path
        server    = local.nfs
        read_only = false
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "gitlab" {
  depends_on = [
    kubernetes_persistent_volume_claim_v1.db, kubernetes_persistent_volume_v1.db, kubernetes_persistent_volume_v1.gitlab
  ]
  metadata {
    name      = "gitlab-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    resources {
      limits = {
        storage = "10Gi"
      }
      requests = {
        storage = "8Gi"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}