locals {
  image_repository = "192.168.1.102:5050"
  nfs              = "192.168.1.102"
  db_nfs_path      = "/volume1/k8s/sourceManagement/db"
  service_nfs_path = "/volume1/k8s/sourceManagement/service"
  runner_nfs_path  = "/volume1/k8s/sourceManagement/runner"
  docker_nfs_path  = "/volume1/k8s/sourceManagement/docker"
}