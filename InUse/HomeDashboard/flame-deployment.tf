resource "kubernetes_deployment_v1" "flame" {
  metadata {
    name      = "flame-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type                     = "service"
      env                      = "prod"
      name                     = "flame"
      "app.kubernetes.io/name" = "homepage"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
      }
    }
    template {
      metadata {
        name   = "flame"
        labels = {
          env  = "prod"
          type = "service"
          name = "flame"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.flame.metadata[0].name
          }
        }
        service_account_name            = "homepage"
        automount_service_account_token = true
        dns_policy                      = "ClusterFirst"
        enable_service_links            = true
        container {
          port {
            container_port = 3000
            name           = "default-port"
          }
          security_context {
            privileged = true
          }
          name              = "flame"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/app/config"
            name       = "data"
          }
        }
      }
    }
  }
}
