resource "kubernetes_service_v1" "flame" {
  metadata {
    name      = "flame-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env                      = "prod"
      type                     = "service"
      name                     = "flame"
      "app.kubernetes.io/name" = "homepage"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
      name = "flame"
    }
    port {
      port        = 80
      target_port = 3000
      name        = "default-port"
    }
  }
}