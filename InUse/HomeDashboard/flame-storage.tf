resource "kubernetes_persistent_volume_v1" "flame" {
  metadata {
    name = "homepage-pv"
  }
  spec {
    storage_class_name = "nfs"
    access_modes       = ["ReadWriteMany"]
    capacity           = {
      storage = "1Gi"
    }
    persistent_volume_source {
      nfs {
        path   = local.service_nfs_path
        server = local.nfs
      }
    }
  }
}


resource "kubernetes_persistent_volume_claim_v1" "flame" {
  metadata {
    name      = "homepage-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    resources {
      limits = {
        storage = "2Gi"
      }
      requests = {
        storage = "1Gi"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}