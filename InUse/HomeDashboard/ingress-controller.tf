resource "kubernetes_ingress_v1" "flame" {
  metadata {
    name        = "flame-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
      #      "gethomepage.dev/description" : "Dynamically Detected Homepage"
      #      "gethomepage.dev/enabled" : "true"
      #      "gethomepage.dev/group" : "Cluster Management"
      #      "gethomepage.dev/icon" : "homepage"
      #      "gethomepage.dev/name" : "Homepage"
    }
    labels = {
      type                     = "ingress"
      env                      = "prod"
      "app.kubernetes.io/name" = "homepage"
    }
  }
  spec {
    tls {
      hosts       = ["home.local"]
      secret_name = "home-local"
    }
    rule {
      host = "home.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.flame.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}