locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_image_digest = "sha256:c7a8fd8c72395cf4120ea2a49267a70e496d5292ac86c750d8111704ac42b87c"
  service_image = "${local.image_repository}/homepage@${local.service_image_digest}"
  service_nfs_path     = "/volume1/k8s/homepage/service"
}