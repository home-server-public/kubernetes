resource "kubernetes_service_account_v1" "homepage" {
  metadata {
    name      = "homepage"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      "app.kubernetes.io/name" = "homepage"
    }
  }
  secret {
    name = "homepage"
  }
}

resource "kubernetes_secret_v1" "homepage" {
  metadata {
    name      = "homepage"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      "app.kubernetes.io/name" = "homepage"
    }
    annotations = {
      "kubernetes.io/service-account.name" = "homepage"
    }
  }
}