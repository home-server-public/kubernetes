resource "kubernetes_ingress_v1" "it-tools" {
  metadata {
    name        = "it-tools-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["it-tools.local"]
      secret_name = "it-tools-local"
    }
    rule {
      host = "it-tools.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.it-tools.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}