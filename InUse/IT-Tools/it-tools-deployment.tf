resource "kubernetes_deployment_v1" "it-tools" {
  metadata {
    name      = "it-tools-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "it-tools"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "it-tools"
      }
    }
    template {
      metadata {
        name   = "it-tools"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "it-tools"
          "app.kubernetes.io/name" = "it-tools"
        }
      }
      spec {
        container {
          port {
            container_port = 80
            name           = "default-port"
          }
          security_context {
            privileged = true
          }
          name              = "it-tools"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
        }
      }
    }
  }
}
