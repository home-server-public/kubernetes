resource "kubernetes_network_policy_v1" "this" {
  metadata {
    name      = "it-tools-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress"]
    pod_selector {
      match_labels = {
        env  = "prod"
        type = "service"
        name = "it-tools"
      }
    }
    ingress {
      ports {
        port     = 80
        protocol = "TCP"
      }
    }
  }
}