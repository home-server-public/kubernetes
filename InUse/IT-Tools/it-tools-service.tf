resource "kubernetes_service_v1" "it-tools" {
  metadata {
    name      = "it-tools-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "it-tools"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 80
      name        = "default-port"
    }
  }
}