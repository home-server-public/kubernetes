locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_image_digest = "sha256:cfabdd7afc6f2e088c57fef9419684c16774bca5aa8fc238ef30c0bc6249819c"
  service_image        = "${local.image_repository}/it-tools@${local.service_image_digest}"
}