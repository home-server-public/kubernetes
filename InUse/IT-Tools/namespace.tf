resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "it-tools"
    labels = {
      env = "prod"
      app = "it-tools"
    }
  }
}