#!/bin/bash

echo "Creating Service Account"
kubectl create sa kube-ds-admin -n kube-system

echo "Creating Role Binding"
kubectl create clusterrolebinding kube-ds-admin-role-binding --clusterrole=admin --user=system:serviceaccount:kube-system:kube-ds-admin

echo "Generating Token"
kubectl create token kube-ds-admin -n kube-system

