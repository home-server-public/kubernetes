resource "kubernetes_secret_v1" "db" {
  metadata {
    name      = "db-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    MYSQL_PASSWORD      = "nocodb"
    MYSQL_ROOT_PASSWORD = "nocodb"
    MYSQL_USER          = "nocodb"
  }
}