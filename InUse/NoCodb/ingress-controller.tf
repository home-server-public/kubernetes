resource "kubernetes_ingress_v1" "nocodb" {
  metadata {
    name        = "nocodb-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
      "nginx.ingress.kubernetes.io/proxy-body-size" : "1024m"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["inventory.local"]
      secret_name = "inventory-local"
    }
    rule {
      host = "inventory.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.nocodb.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}