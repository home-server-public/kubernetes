locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  db_nfs_path          = "/volume1/k8s/nocodb/db"
  service_nfs_path     = "/volume1/k8s/nocodb/service"
  db_image_digest      = "sha256:4c59990ae8966b52330f64f5202a6e9bd7e1d6bfb89dd69583e24d9d0e9bcd26"
  service_image_digest = "sha256:4fb65855d6da3eb574fffddb1f810f215ef0ff827a5c7b92afda37e91c8544e2"
  db_image             = "${local.image_repository}/mariadb@${local.db_image_digest}"
  service_image        = "${local.image_repository}/nocodb@${local.service_image_digest}"
}