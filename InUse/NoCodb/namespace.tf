resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "inventory-tracker"
    labels = {
      env = "prod"
      app = "inventory"
    }
  }
}