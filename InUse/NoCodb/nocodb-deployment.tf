resource "kubernetes_deployment_v1" "nocodb" {
  metadata {
    name      = "nocodb"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "nocodb"

    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "nocodb"
      }
    }
    template {
      metadata {
        name   = "nocodb"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "nocodb"
          "app.kubernetes.io/name" = "nocodb"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.nocodb.metadata[0].name
          }
        }
        container {
          name              = "nocodb"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 8080
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.nocodb.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.nocodb.metadata[0].name
              optional = false
            }
          }
          volume_mount {
            mount_path = "/usr/app/data"
            name       = "data"
          }
          resources {
            requests = {
              cpu    = "256m"
              memory = "1Gi"
            }
            limits = {
              cpu    = "1024m"
              memory = "2Gi"
            }
          }
        }
      }
    }
  }
}
