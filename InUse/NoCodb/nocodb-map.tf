resource "kubernetes_config_map_v1" "nocodb" {
  metadata {
    name      = "nocodb-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    NC_DB: "mysql2://nocodb-db.inventory-tracker:3306?u=nocodb&p=nocodb&d=nocodb"
  }
}