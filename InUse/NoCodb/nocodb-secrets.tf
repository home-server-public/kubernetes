resource "kubernetes_secret_v1" "nocodb" {
  metadata {
    name      = "nocodb-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    DB_USERNAME = "nocodb"
    DB_PASSWORD = "nocodb"
  }
}