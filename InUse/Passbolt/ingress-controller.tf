resource "kubernetes_ingress_v1" "passbolt" {
  metadata {
    name        = "passbolt-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["secrets.local"]
      secret_name = "secrets-local"
    }
    rule {
      host = "secrets.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.passbolt.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}