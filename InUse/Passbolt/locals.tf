locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  db_image_digest      = "sha256:4c59990ae8966b52330f64f5202a6e9bd7e1d6bfb89dd69583e24d9d0e9bcd26"
  service_image_digest = "sha256:8e4131c48d46e35e03e0e6da5ca51eba6470d8aaccc27d1191947f6337ee93d6"
  db_image             = "${local.image_repository}/mariadb@${local.db_image_digest}"
  service_image        = "${local.image_repository}/passbolt@${local.service_image_digest}"
  db_nfs_path          = "/volume1/k8s/passbolt/db"
  service_nfs_path     = "/volume1/k8s/passbolt/service"
}