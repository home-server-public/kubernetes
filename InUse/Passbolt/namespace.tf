resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "pass-bolt"
    labels = {
      env = "prod"
      app = "secrets"
    }
  }
}