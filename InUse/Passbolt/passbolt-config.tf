resource "kubernetes_config_map_v1" "passbolt-sys-config" {
  metadata {
    name      = "passbolt-php-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    "passbolt.php" = <<EOF
<?php
return [
    'App' => [
        'fullBaseUrl' => 'https://secrets.local',
    ],

    // Database configuration.
    'Datasources' => [
        'default' => [
            'driver' => 'Cake\\Database\\Driver\\Mysql',
            'host' => 'passbolt-db.pass-bolt',
            'port' => '3306',
            'username' => 'admin',
            'password' => 'admin',
            'database' => 'passbolt',
        ],
    ],

    'passbolt' => [
        'gpg' => [
            'serverKey' => [
                // Server private key fingerprint.
                'fingerprint' => '362B06FB4795E79C6FB8DD7C48CFB4E125757F2A',
                'public' => CONFIG . DS . 'gpg' . DS . 'serverkey.asc',
                'private' => CONFIG . DS . 'gpg' . DS . 'serverkey_private.asc',
            ],
        ],
        'ssl' => [
            'force' => false,
        ]
    ],
];
EOF
  }
}

