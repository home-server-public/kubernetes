resource "kubernetes_deployment_v1" "passbolt" {
  depends_on = [kubernetes_deployment_v1.db]
  metadata {
    name      = "passbolt"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "passbolt"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "passbolt"
      }
    }
    template {
      metadata {
        name   = "passbolt"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "passbolt"
          "app.kubernetes.io/name" = "passbolt"
        }
      }
      spec {
        volume {
          name = "config"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.passbolt.metadata[0].name
          }
        }
        volume {
          name = "init-config"
          config_map {
            name = kubernetes_config_map_v1.passbolt-sys-config.metadata[0].name
          }
        }
        container {
          name              = "passbolt"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 8080
            name           = "default-port"
          }
          port {
            container_port = 1025
            name           = "smtp-port"
          }
          security_context {
            privileged = true
          }
          volume_mount {
            mount_path = "/etc/passbolt/passbolt.php"
            sub_path   = "passbolt.php"
            name       = "init-config"
          }
          volume_mount {
            mount_path = "/etc/passbolt/gpg"
            name       = "config"
          }
          volume_mount {
            mount_path = "/etc/passbolt/jwt"
            name       = "config"
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.passbolt.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.passbolt.metadata[0].name
              optional = false
            }
          }
        }
      }
    }
  }
}
