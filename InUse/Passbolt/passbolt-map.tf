resource "kubernetes_config_map_v1" "passbolt" {
  metadata {
    name      = "passbolt-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    APP_FULL_BASE_URL           = "https://secrets.local"
    DATASOURCE_DEFAULT_DATABASE = "passbolt"
    DATASOURCE_DEFAULT_HOST     = "passbolt-db.pass-bolt"
  }
}