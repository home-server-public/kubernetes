resource "kubernetes_network_policy_v1" "this" {
  metadata {
    name      = "passbolt-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress", "Egress"]
    pod_selector {
      match_labels = {
        env  = "prod"
        type = "service"
        name = "passbolt"
      }
    }
    ingress {
      ports {
        port     = 8080
        protocol = "TCP"
      }
      ports {
        port     = 80
        protocol = "TCP"
      }
    }
    egress {
      ports {
        port     = 3306
        protocol = "TCP"
      }
      ports {
        port     = 25
        protocol = "TCP"
      }
      # Allow requests to coreDNS to resolve DB by service name
      ports {
        port     = 53
        protocol = "UDP"
      }
      to {
        ip_block {
          cidr = "192.168.1.102/32"
        }
      }
      to {
        # Allow requests to coreDNS to resolve DB by service name
        namespace_selector {
          match_labels = {
            "kubernetes.io/metadata.name" = "kube-system"
          }
        }
        pod_selector {
          match_labels = {
            "k8s-app" = "kube-dns"
          }
        }
      }
      to {
        pod_selector {
          match_labels = {
            type = "db"
            env  = "prod"
          }
        }
      }
    }
  }
}