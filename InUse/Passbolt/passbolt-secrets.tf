resource "kubernetes_secret_v1" "passbolt" {
  metadata {
    name      = "passbolt-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    DATASOURCE_DEFAULT_PASSWORD = "admin"
    DATASOURCE_DEFAULT_USERNAME = "admin"
  }
}