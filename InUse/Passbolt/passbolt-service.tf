resource "kubernetes_service_v1" "passbolt" {
  metadata {
    name      = "passbolt-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "passbolt"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port = 80
      name = "default-port"
    }
  }
}