resource "kubernetes_deployment_v1" "db" {
  depends_on = [kubernetes_config_map_v1.db]
  metadata {
    name      = "kimai-db"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "db"
      env  = "prod"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "db"
        env  = "prod"
      }
    }
    template {
      metadata {
        name   = "kimai-db"
        labels = {
          type = "db"
          env  = "prod"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.db.metadata[0].name
          }
        }
        security_context {
          run_as_user = "1026"
        }
        container {
          security_context {
            run_as_user = "1026"
          }
          name              = "mariadb"
          image             = local.db_image
          image_pull_policy = "IfNotPresent"
          volume_mount {
            mount_path = "/var/lib/mysql"
            name       = "data"
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.db.metadata[0].name
              optional = false
            }
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.db.metadata[0].name
              optional = false
            }
          }
          port {
            container_port = 3306
            protocol       = "TCP"
          }
        }
      }
    }
  }
}
