resource "kubernetes_persistent_volume_v1" "db" {
  metadata {
    name = "kimai-db-pv"
  }
  spec {
    storage_class_name = "nfs"
    access_modes       = ["ReadWriteMany"]
    capacity           = {
      storage = "10Gi"
    }
    persistent_volume_source {
      nfs {
        path   = local.db_nfs_path
        server = local.nfs
      }
    }
  }
}


resource "kubernetes_persistent_volume_claim_v1" "db" {
  metadata {
    name      = "kimai-db-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    resources {
      limits = {
        storage = "20Gi"
      }
      requests = {
        storage = "10Gi"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}