resource "kubernetes_deployment_v1" "kimai" {
  metadata {
    name      = "kimai"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "kimai"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "kimai"
      }
    }
    template {
      metadata {
        name   = "kimai"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "kimai"
          "app.kubernetes.io/name" = "kimai"
        }
      }
      spec {
        container {
          name              = "kimai"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          port {
            container_port = 8001
          }
          env_from {
            secret_ref {
              name     = kubernetes_secret_v1.kimai.metadata[0].name
              optional = false
            }
          }
          env_from {
            config_map_ref {
              name     = kubernetes_config_map_v1.kimai.metadata[0].name
              optional = false
            }
          }
        }
      }
    }
  }
}
