resource "kubernetes_config_map_v1" "kimai" {
  metadata {
    name      = "kimai-configmap"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    DATABASE_URL = "mysql://kimai:kimai@kimai-db:3306/kimai?charset=utf8"
  }
}