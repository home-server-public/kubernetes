resource "kubernetes_network_policy_v1" "this" {
  metadata {
    name      = "kimai-network-policy"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    policy_types = ["Ingress", "Egress"]
    pod_selector {
      match_labels = {
        env  = "prod"
        type = "service"
        name = "kimai"
      }
    }
    ingress {
      ports {
        port     = 8001
        protocol = "TCP"
      }
    }
    egress {
      ports {
        port     = 3306
        protocol = "TCP"
      }
      # Allow requests to coreDNS to resolve DB by service name
      ports {
        port     = 53
        protocol = "UDP"
      }
      to {
        # Allow requests to coreDNS to resolve DB by service name
        namespace_selector {
          match_labels = {
            "kubernetes.io/metadata.name" = "kube-system"
          }
        }
        pod_selector {
          match_labels = {
            "k8s-app" = "kube-dns"
          }
        }
      }
      to {
        pod_selector {
          match_labels = {
            type = "db"
            env  = "prod"
          }
        }
      }
    }
  }
}