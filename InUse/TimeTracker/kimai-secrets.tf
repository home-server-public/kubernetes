resource "kubernetes_secret_v1" "kimai" {
  metadata {
    name      = "kimai-secrets"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  data = {
    MYSQL_PASSWORD      = "kimai"
    MYSQL_ROOT_PASSWORD = "kimai"
  }
}