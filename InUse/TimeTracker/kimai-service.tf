resource "kubernetes_service_v1" "kimai" {
  metadata {
    name      = "kimai-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "kimai"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
      name = "kimai"
    }

    port {
      port        = 80
      target_port = 8001
    }
  }
}