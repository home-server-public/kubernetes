locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  db_image_digest      = "sha256:4c59990ae8966b52330f64f5202a6e9bd7e1d6bfb89dd69583e24d9d0e9bcd26"
  service_image_digest = "sha256:ef10d16cd5f7c2e812e1121b1c3cf86e2cdf15ca8b729108dc11e431264c2b8c"
  db_image             = "${local.image_repository}/mariadb@${local.db_image_digest}"
  service_image        = "${local.image_repository}/kimai@${local.service_image_digest}"
  db_nfs_path          = "/volume1/k8s/kimai/db"
  service_nfs_path     = "/volume1/k8s/kimai/service"
}