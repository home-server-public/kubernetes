resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "time-tracker"
    labels = {
      app = "time-tracker"
      env = "prod"
    }
  }
}