locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_image_digest = "sha256:b46bc95022e3c2c94fc8ad293e15c82b750bb7aeefa4a257d2f794c6d9c5e503"
  service_image        = "${local.image_repository}/dufs@${local.service_image_digest}"
  service_nfs_path     = "/volume1/k8s/webserver/data"
}