resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "file-share"
    labels = {
      env = "prod"
    }
  }
}