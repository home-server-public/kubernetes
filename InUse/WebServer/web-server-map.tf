resource "kubernetes_config_map_v1" "this" {
  metadata {
    name      = "web-server-map"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env = "prod"
    }
  }

  data = {
    DUFS_ALLOW_UPLOAD : "true"
    DUFS_ALLOW_DELETE : "true"
    DUFS_ALLOW_SEARCH : "true"
    DUFS_ALLOW_SYMLINK : "true"
    DUFS_ALLOW_ARCHIVE : "true"
  }
}