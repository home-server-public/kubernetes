resource "kubernetes_deployment_v1" "this" {
  metadata {
    name      = "file-share-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "file-share"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "file-share"
      }
    }
    template {
      metadata {
        name   = "file-share"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "file-share"
          "app.kubernetes.io/name" = "file-share"
        }
      }
      spec {
        volume {
          name = "data"
          persistent_volume_claim {
            claim_name = kubernetes_persistent_volume_claim_v1.webserver.metadata[0].name
          }
        }
        security_context {
          run_as_user = "0"
          fs_group    = "0"
        }
        container {
          port {
            container_port = 5000
            protocol       = "TCP"
            name           = "default-port"
          }
          name              = "file-share"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
          env_from {
            config_map_ref {
              name = kubernetes_config_map_v1.this.metadata[0].name
            }
          }
          volume_mount {
            mount_path = "/data"
            name       = "data"
          }
        }
      }
    }
  }
}
