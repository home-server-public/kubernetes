resource "kubernetes_persistent_volume_v1" "webserver" {
  metadata {
    name   = "file-share-pv"
    labels = {
      app = "coder"
    }
  }
  spec {
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
    capacity           = {
      storage = "500Gi"
    }
    persistent_volume_source {
      nfs {
        path      = local.service_nfs_path
        server    = local.nfs
        read_only = false
      }
    }
  }
}

resource "kubernetes_persistent_volume_claim_v1" "webserver" {
  depends_on = [kubernetes_persistent_volume_v1.webserver]
  metadata {
    name      = "file-share-pvc"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
  }
  spec {
    selector {
      match_labels = {
        app = "coder"
      }
    }
    resources {
      limits = {
        storage = "1Ti"
      }
      requests = {
        storage = "500Gi"
      }
    }
    access_modes       = ["ReadWriteMany"]
    storage_class_name = "nfs"
  }
}