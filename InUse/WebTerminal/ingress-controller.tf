resource "kubernetes_ingress_v1" "cloudbeaver" {
  metadata {
    name        = "webterminal-ingress"
    namespace   = kubernetes_namespace_v1.this.metadata[0].name
    annotations = {
      "cert-manager.io/cluster-issuer" : "selfsigned-cluster-issuer"
    }
    labels = {
      type = "ingress"
      env  = "prod"
    }
  }
  spec {
    tls {
      hosts       = ["webterminal.local"]
      secret_name = "webterminal-local"
    }
    rule {
      host = "webterminal.local"
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service_v1.webterminal.metadata[0].name
              port {
                number = 443
              }
            }
          }
        }
      }
    }
  }
}