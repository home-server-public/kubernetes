locals {
  image_repository     = "192.168.1.102:5050"
  nfs                  = "192.168.1.102"
  service_nfs_path     = "/volume1/k8s/sshwifty/service"
  key_digest           = "SHA256:2LPlA0xYdVixIL9LQXn8I/wjHYnmi3VflBfuI8iY4yc"
  service_image_digest = "sha256:c147e0d7620ca2c74e2936072e26b7f8d98b62758760173eb33d16f52319335e"
  service_image        = "${local.image_repository}/sshwifty@${local.service_image_digest}"
}