resource "kubernetes_namespace_v1" "this" {
  metadata {
    name   = "webterminal"
    labels = {
      env = "prod"
      app = "webterminal"
    }
  }
}