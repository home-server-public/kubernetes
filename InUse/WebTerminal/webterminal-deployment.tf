resource "kubernetes_deployment_v1" "webterminal" {
  metadata {
    name      = "webterminal-deployment"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      type = "service"
      env  = "prod"
      name = "webterminal"
    }
  }
  spec {
    selector {
      match_labels = {
        type = "service"
        env  = "prod"
        name = "webterminal"
      }
    }
    template {
      metadata {
        name   = "webterminal"
        labels = {
          env                      = "prod"
          type                     = "service"
          name                     = "webterminal"
          "app.kubernetes.io/name" = "webterminal"
        }
      }
      spec {
        container {
          port {
            container_port = 8182
            name           = "default-port"
          }
          security_context {
            privileged = true
          }
          name              = "webterminal"
          image             = local.service_image
          image_pull_policy = "IfNotPresent"
        }
      }
    }
  }
}
