resource "kubernetes_service_v1" "webterminal" {
  metadata {
    name      = "webterminal-service"
    namespace = kubernetes_namespace_v1.this.metadata[0].name
    labels    = {
      env  = "prod"
      type = "service"
      name = "webterminal"
    }
  }
  spec {
    selector = {
      env  = "prod"
      type = "service"
    }

    port {
      port        = 443
      target_port = 8182
      name        = "default-port"
    }
  }
}